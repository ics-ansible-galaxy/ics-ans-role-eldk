import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    assert host.file('/export/sdk/eldk-5.6/ifc1210/sysroots').is_directory


def test_opt_eldk_link(host):
    assert host.file('/opt/eldk-5.6').linked_to == '/export/sdk/eldk-5.6'
